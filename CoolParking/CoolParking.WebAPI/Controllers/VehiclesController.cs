﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private static IParkingService parkingService;
        public  VehiclesController(IParkingService service)
        {
            parkingService = service;
        }
        // GET: api/vehicles
        [HttpGet]
        [Route("vehicles")]
        public ActionResult<IEnumerable<Vehicle>> GetVehicles()
        {
            return Ok(parkingService.GetVehicles());
        }

        // GET: api/vehicles/{id}
        [HttpGet]
        [Route("vehicles/{id}")]
        public ActionResult<Vehicle> GetVehicleById([FromRoute]string id)
        {
            try
            {
                return Ok(parkingService.GetVehicles().Single(v => v.Id == id));
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        }

        // POST: api/vehicles
        [HttpPost]
        [Route("vehicles")]
        public ActionResult AddVehicle([FromBody] Vehicle vehicle)
        {
            try
            {
                parkingService.AddVehicle(vehicle);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(JsonConvert.SerializeObject(ex.Message));
            }
        }

        // DELETE: api/vehicles/{id}
        [HttpDelete]
        [Route("vehicles/{id}")]
        public ActionResult Delete([FromRoute]string id)
        {
            try
            {
                parkingService.RemoveVehicle(id);
                return Ok();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(JsonConvert.SerializeObject(ex.Message));
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(JsonConvert.SerializeObject(ex.Message));
            }
        }
    }
}
