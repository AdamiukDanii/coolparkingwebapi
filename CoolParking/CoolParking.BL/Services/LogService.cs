﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.Dynamic;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string logFilePath;
        public string LogPath { get; }
        public LogService(string logFilePath)
        {
            this.logFilePath = logFilePath;


        }


        public string Read()
        {
            if (File.Exists(logFilePath) == false) throw new InvalidOperationException("File doesn't exist");
            string logData;
            using (StreamReader sr = new StreamReader(logFilePath))
            {
               logData =  sr.ReadToEnd();
            }
            return logData;
        }

        public void Write(string logInfo)
        {
            using (StreamWriter sw = new StreamWriter(logFilePath,true))
            {
                
                sw.WriteLine(logInfo);
            }
        }
    }
}