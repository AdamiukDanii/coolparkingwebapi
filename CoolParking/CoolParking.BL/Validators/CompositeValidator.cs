﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Validators
{
    class CompositeValidator : IValidate
    {
        List<IValidate> list = new List<IValidate>();
        public bool Validate()
        {
            bool result;
            foreach (var o in list)
            {
                result = o.Validate();
                if (result == false) return result;
            }
            return true;
        }

        internal void Add(IValidate obj)
        {
            list.Add(obj);
        }

        internal void Clear()
        {
            list.Clear();
        }
    }
}
