﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoolParking.BL.Validators
{
    class IsVehicleBalancePositive : IValidate
    {
        Parking _parking;
        string _vehicleId;
        public IsVehicleBalancePositive(Parking parking, string vehicleId)
        {
            _parking = parking;
            _vehicleId = vehicleId;
        }
        public bool Validate()
        {
            return _parking.ParkedVehicles.Any(v => v.Id == _vehicleId&&  v.Balance > 0);
        }
    }
}
