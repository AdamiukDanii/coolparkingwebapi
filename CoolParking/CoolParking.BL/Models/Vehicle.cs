﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
using CoolParking.BL.Validators;
using System;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType VehicleType, decimal balance)
        {
            if (new IsIdFormatOk(id).Validate() == false || balance < 0) throw new ArgumentException("Input data is invalid");
            this.Id = id;
            this.VehicleType = VehicleType;
            this.Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random rand = new Random();
            string numbers = null;
            string plateNumber = null;
            for(int i =0; i<4; i++)
            {
                numbers += rand.Next(0, 9).ToString();
                plateNumber += ((char)rand.Next('A', 'Z' + 1)).ToString();
            }
            
            return plateNumber.Insert(2, "-" + numbers + "-");
        }
    }



}
