﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static string logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        public static decimal InitialParkingBalance = 0;
        public static int ParkingCapacity = 10;
        public static int PaymentPeriod = 5000;
        public static int LoggingPeriod = 60000;
        public static decimal PenaltyMultiplier = 2.5M;
        public static Dictionary<VehicleType, decimal> Tariffs = new Dictionary<VehicleType, decimal>
        {
            {VehicleType.PassengerCar , 2},
            {VehicleType.Truck, 5 },
            {VehicleType.Bus, 3.5M },
            {VehicleType.Motorcycle, 1 }
        };
    }
}