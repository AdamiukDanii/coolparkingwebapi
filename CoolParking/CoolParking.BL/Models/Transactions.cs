﻿using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Models
{
    internal class Transactions
    {
        public static List<TransactionInfo> LastParkingTransactions = new List<TransactionInfo>();


        LogService lg = new LogService(Settings.logFilePath);
        
        public void WriteLog(object sender, ElapsedEventArgs e)
        {
            foreach(TransactionInfo t in LastParkingTransactions.ToList())
            {
                lg.Write($"Vehicle: {t.Id}, Sum: {t.Sum}, Date: {t.TransactionDate}  ");
            }
            Transactions.LastParkingTransactions.Clear();
            
        }
    }
}
