﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
using System;
using System.Collections.Generic;
using System.Timers;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        private Parking()
        {
            CurrentParkingBalance = Settings.InitialParkingBalance;
            CurrentParkingCapacity = Settings.ParkingCapacity;
        }

        private static readonly Lazy<Parking> lazy =
          new Lazy<Parking>(() => new Parking());

        public static Parking getParkingInstance { get { return lazy.Value; } }

        internal decimal CurrentParkingBalance { get; set; }
        internal int CurrentParkingCapacity { get; set; }
        internal List<Vehicle> ParkedVehicles = new List<Vehicle>();
      

        public void Withdraw(object sender, ElapsedEventArgs e)
        {
            foreach (Vehicle v in ParkedVehicles)
            {
                decimal withdraw = 0;
                if (v.Balance < Settings.Tariffs[v.VehicleType] && v.Balance > 0)
                {
                    // new vechile balance = current balance + |tariff payment - current ballance | * penalty coef.
                    withdraw = v.Balance + Math.Abs(Settings.Tariffs[v.VehicleType] - v.Balance) * Settings.PenaltyMultiplier;
                }
                if (v.Balance < 0)
                {
                    withdraw = Settings.Tariffs[v.VehicleType] * Settings.PenaltyMultiplier;
                }
                else withdraw = Settings.Tariffs[v.VehicleType];
                v.Balance -= withdraw;
                CurrentParkingBalance += withdraw;
                Transactions.LastParkingTransactions.Add(new TransactionInfo { TransactionDate = DateTime.Now, Id = v.Id, Sum = withdraw });
                
            }
            
        }

    }

}
