﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleInterface.Models
{
    class Vehicle
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }
        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }

    public enum VehicleType
    {
        PassengerCar = 1,
        Truck = 2,
        Bus = 3,
        Motorcycle = 4
    }
}
