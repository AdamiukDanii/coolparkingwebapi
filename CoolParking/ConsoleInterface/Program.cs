﻿using System;
using System.Linq;
using System.Linq.Expressions;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.Validators;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ConsoleInterface
{
    class Program
    {
        static HttpClient client;
        static void Main(string[] args)
        {
            client = new HttpClient();


            Console.WriteLine("Hi, welcome to Cool Parking. \nThis program allows you to automate the routine processes of parking.");
            Menu();
        }

        #region REST
        public static object GetObject(string route)
        {
            var result = client.GetStringAsync($"https://localhost:44397/api/{route}");
            return JsonConvert.DeserializeObject(result.Result);
        }

        public static void PostObject(object obj, string route)
        {
            var postObj = JsonConvert.SerializeObject(obj);
            client.PostAsync($"https://localhost:44397/api/{route}",
                                new StringContent(postObj, System.Text.Encoding.UTF8, "application/json"));
        }

        public static void DeleteObject(string id, string route)
        {
            client.DeleteAsync($"https://localhost:44397/api/{route}/{id}");
        }

        public static void PutObject(object obj, string route)
        {
            var putObj = JsonConvert.SerializeObject(obj);
            client.PutAsync($"https://localhost:44397/api/{route}",
                        new StringContent(putObj, System.Text.Encoding.UTF8, "application/json"));
        }
        #endregion
        public static Vehicle CreateVehicle()
        {
            decimal sum;
            VehicleType vehicleType = 0;
            Console.Write("\nEnter vehicle plate number in format (AB-1234-CD): ");
            string vehicleId = Console.ReadLine();
            Console.Write("\nEnter initial balance: ");
            sum = Convert.ToDecimal(Console.ReadLine());
            Console.Write("\nEnter vehicle type number (1-Passenger Car, 2-Truck, 3-Bus, 4-Motorcycle): ");
            string vType = Console.ReadLine();
            switch (vType)
            {
                case "1":
                    vehicleType = VehicleType.PassengerCar;
                    break;
                case "2":
                    vehicleType = VehicleType.Truck;
                    break;
                case "3":
                    vehicleType = VehicleType.Bus;
                    break;
                case "4":
                    vehicleType = VehicleType.Motorcycle;
                    break;
                default:
                    Console.WriteLine("Invalid vehicle type");
                    break;
            }
            if (vehicleType != 0)
            {
                return new Vehicle(vehicleId, vehicleType, sum);
            }
            return null;
        }

        public static TransactionInfo CreateTransaction()
        {
            Console.Write("\nEnter vehicle plate number in format (AB-1234-CD): ");
           string vehicleId = Console.ReadLine();
            Console.Write("\nEnter a top up sum: ");
           decimal sum = Convert.ToDecimal(Console.ReadLine());
            return new TransactionInfo { Id = vehicleId, Sum = sum };
        }

        static void Menu()
        {
            string command = null;

            #region Menu items
            Console.WriteLine(new string('#', 60));
            Console.WriteLine("#" + new string(' ', 27) + "MENU" + new string(' ', 27) + "#");
            Console.WriteLine(new string('#', 60));
            Console.WriteLine("1. Check the current parking balance");
            Console.WriteLine("2. Check the number of free parking spaces.");
            Console.WriteLine("3. Display all parking transactions for the current period");
            Console.WriteLine("4. Display transaction history");
            Console.WriteLine("5. Display the list of Vehicles in the parking ");
            Console.WriteLine("6. Display the vehicle by ID");
            Console.WriteLine("7. Put the vehicle to the Parking");
            Console.WriteLine("8. Take the vehicle out of the parking");
            Console.WriteLine("9. Top up the balance of a vehicle.");
            Console.WriteLine("10. Exit.");
            #endregion

            while (command != "10")
            {
                Console.Write("\nTo make a selection, enter the menu option number: ");
                command = Console.ReadLine();

                try
                {
                    switch (command)
                    {
                        case "1":
                            Console.WriteLine("Parking balance: " + GetObject("parking/balance"));
                            break;

                        case "2":
                            Console.WriteLine($"Free: {GetObject("parking/freePlaces")} places");
                            break;
                        case "3":
                            Console.WriteLine(GetObject("transactions/last"));
                            break;
                        case "4":
                            Console.WriteLine(GetObject("transactions/all"));
                            break;
                        case "5":
                            Console.WriteLine(GetObject("vehicles"));
                            break;
                        case "6":
                            string vehicleId;
                            Console.Write("\nEnter vehicle plate number in format (AB-1234-CD): ");
                            vehicleId = Console.ReadLine();
                            Console.WriteLine(GetObject($"vehicles/{vehicleId}"));
                            break;
                        case "7":
                            Vehicle vehicle = CreateVehicle();
                            PostObject(vehicle, "vehicles");
                            break;
                        case "8":
                            Console.Write("\nEnter vehicle plate number in format (AB-1234-CD): ");
                            vehicleId = Console.ReadLine();
                            DeleteObject(vehicleId, "vehicles");
                            Console.WriteLine("Vehicle is successfully removed");
                            break;
                        case "9":
                            TransactionInfo transaction = CreateTransaction();
                            PutObject(transaction, "transactions/topUpVehicle");
                            Console.WriteLine("Ballance is topped up");
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please, try again");
                    Menu();
                }
            }
        }
    }
}
